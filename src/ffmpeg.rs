use std::process::Command;

const FFMPEG_CMD: &str = "ffmpeg";

pub enum FFmpegStatus {
	NotFound,
	Present,
}

pub fn check_availability() -> FFmpegStatus {
	let result = Command::new(FFMPEG_CMD)
		.arg("-hide_banner")
		.arg("--help")
		.output();
	match result {
		Ok(_result) => FFmpegStatus::Present,
		Err(err) => {
			println!("Error: {err}");
			FFmpegStatus::NotFound
		}
	}
}
