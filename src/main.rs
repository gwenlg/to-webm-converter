mod ffmpeg;

use adw::prelude::*;
use ffmpeg::FFmpegStatus;
use gtk::{glib, Orientation};

const APP_ID: &str = "fr.gwenlg.ToWebMConverter";

fn main() -> glib::ExitCode {
	let app = adw::Application::builder().application_id(APP_ID).build();

	app.connect_activate(build_ui);
	app.run()
}

fn build_ui(app: &adw::Application) {
	let content = gtk::Box::new(Orientation::Vertical, 0);
	let headerbar = adw::HeaderBar::builder().build();
	content.append(&headerbar);

	match ffmpeg::check_availability() {
		FFmpegStatus::NotFound => {
			content.append(&build_status_missing_ffmpeg());
		}
		FFmpegStatus::Present => {
			content.append(&build_status());
		}
	}

	let window = adw::ApplicationWindow::builder()
		.application(app)
		.title("To WebM Converter")
		.width_request(640)
		.height_request(480)
		.content(&content)
		.build();

	window.present();
}

fn build_status() -> adw::StatusPage {
	let status_page = adw::StatusPage::builder()
		.title("Status")
		.icon_name("camera-video-symbolic")
		.description("Un outil pour facilement convertir des video en webm avec ffmpeg")
		.build();
	status_page
}

fn build_status_missing_ffmpeg() -> adw::StatusPage {
	let status_page = adw::StatusPage::builder()
		.title("Problème")
		.icon_name("dialog-error-symbolic")
		.description("Ffmpeg n'est pas disponible, veuillez l'installer et relancer l'application")
		.build();
	status_page
}
